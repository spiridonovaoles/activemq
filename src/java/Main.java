/**
 * Created by Olesya on 12.05.2018.
 */
public class Main {

    public static void main(String[] args) {

        Thread producerThread = new Thread(() -> {
            MyMessageProvider producer = new MyMessageProvider();

            //producer.send("Hello");
        });

        Thread consumerThread = new Thread(() -> {
            MyMessageConsumer consumer = new MyMessageConsumer();

            //consumer.recieve();
        });

        producerThread.start();
        consumerThread.start();


        try {
            producerThread.join();
            consumerThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
