import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 * Created by Olesya on 12.05.2018.
 */
public class MyMessageConsumer {
    public Connection createConnection() throws JMSException {
        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory("tcp://localhost:61616");
        return factory.createConnection();
    }

    public MyMessage receive() {
        MyMessage myMessage = null;
        try {
            Connection connection = createConnection();
            connection.start();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Destination destination = session.createQueue("TestQueue");

            MessageConsumer consumer = session.createConsumer(destination);

            Message msg = consumer.receive(10000);
            myMessage = new MyMessage(MessageType.TEXT, ((TextMessage) msg).getText());
            System.out.println(((TextMessage) msg).getText());
            session.close();
            connection.close();
        } catch (JMSException e) {
            e.printStackTrace();
        }
        return myMessage;
    }
}
