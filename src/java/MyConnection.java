import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

public class MyConnection {
    public MyConnection() {
        TopicConnection connection = null;
        try {
            connection = createConnection();
            connection.start();
        } catch (JMSException e) {
            e.printStackTrace();
        }

    }

    public TopicConnection createConnection() throws JMSException {
        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory("tcp://localhost:61616");
        return factory.createTopicConnection();
    }

    public MyMessage receive() {
        MyMessage myMessage = null;
        try {
            Connection connection = createConnection();
            connection.start();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Destination destination = session.createQueue("TestQueue");

            MessageConsumer consumer = session.createConsumer(destination);

            Message msg = consumer.receive(10000);
            myMessage = new MyMessage(MessageType.TEXT, ((TextMessage) msg).getText());
            System.out.println(((TextMessage) msg).getText());
            session.close();
            connection.close();
        } catch (JMSException e) {
            e.printStackTrace();
        }
        return myMessage;
    }

    public void send(MyMessage message) {
        try {
            Connection connection = createConnection();
            connection.start();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Destination destination = session.createQueue("TestQueue");
            MessageProducer producer = session.createProducer(destination);
            //DeliveryMode.NON_PERSISTENT отличаются в скорости доставки, персистентные дублируются копией на ЖД
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

            TextMessage textMessage = session.createTextMessage(message.getData());

            producer.send(textMessage);
            try {
                while (true) {
                    Thread.sleep(20000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        } catch (JMSException e) {
            e.printStackTrace();
        }
    }


/*    @Override
    public void close() throws IOException
    {
        session.close();
        connection.close();
    }*/
}
