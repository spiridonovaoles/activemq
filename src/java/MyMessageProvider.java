import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 * Created by Olesya on 12.05.2018.
 */
public class MyMessageProvider {

    public TopicConnection createConnection() throws JMSException {
        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory("tcp://localhost:61616");
        return factory.createTopicConnection();
    }

    public void send(MyMessage message) {
        try {
            Connection connection = createConnection();
            connection.start();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Destination destination = session.createQueue("TestQueue");
            MessageProducer producer = session.createProducer(destination);
            //DeliveryMode.NON_PERSISTENT отличаются в скорости доставки, персистентные дублируются копией на ЖД
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

            TextMessage textMessage = session.createTextMessage(message.getData());

            producer.send(textMessage);
            try {
                while (true) {
                    Thread.sleep(20000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
